-- SUMMARY --

Provides a block with form to search for domains available for registration.


-- REQUIREMENTS --

None.


-- INSTALLATION --

* Install as usual, see http://drupal.org/node/70151 for further information.


-- CONFIGURATION --

* Configure the Whois Program and enabled Domain Extentions:

  - Access "Configuration > Web services > Check Domain Availability"

    http://www.example.com/admin/config/services/check_domain_availability

  - Use the Block Administration or Panels to place the Form
